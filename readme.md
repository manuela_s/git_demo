# Workshop
*Git* demo as part of the workshop ["Version Control with GIT"](https://staff.rcsi.ie/upcoming-courses/research-data-science/version-control-with-git) held as part of the module "PC10 Optional: Fundamentals of Computational Biology, 2019-2020), (RCSI Dublin, 2019-11-06).

## Materials
- Presentation slides:
    - [html format](https://manuelas.bitbucket.io/talk/gitworkshop/presentation.html)
    - [pdf format](https://manuelas.bitbucket.io/talk/gitworkshop/presentation.pdf)
- Hands-on examples:
    - [handout](https://manuelas.bitbucket.io/talk/gitworkshop/handout.pdf)
    - [solutions](https://manuelas.bitbucket.io/talk/gitworkshop/solutions.pdf)

## Contact
For further information, please get in touch: manuelasalvucci@rcsi.ie

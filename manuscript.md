# Abstract
This is going to be a great paper about a great topic.

# Introduction

# Materials and Methods
## Samples.

We used N=100 patient samples.

## RNASeq.

## Immunohistochemistry.

## Bioinformatics.

Some very complicated analysis was performed.

## Statistical analysis.

# Results

# Conclusions

# References

# Figures and tables
- ![Figure1, schematic representation of the analysis pipeline](figures/figure1.png)
- ![Figure2, differential expression analysis](figures/figure2.png)
- ![Table1, clinical features](figures/table1.jpg)

